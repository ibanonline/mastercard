function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
function removeClass(e,c) {e.className = e.className.replace( new RegExp('(?:^|\\s)'+c+'(?!\\S)') ,'');}
function addClass(e,c) { // only add class if not already added
    if(e.className.match(new RegExp('\\b'+c+'\\b')) == null) {
        e.className += ' '+c;
    }
}
function hasClass(e, c) {
    if(e.className.match(new RegExp('\\b'+c+'\\b')) != null) {
        return true;
    }
    return false;
}

function formatMoney(money) {
    var formattedMoney = money;

    // http://stackoverflow.com/a/2901298
    formattedMoney = Math.round(formattedMoney);
    formattedMoney = formattedMoney.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    return formattedMoney;
}

function addMonths(date, months) {
    var result = new Date(date);
    result.setMonth(result.getMonth() + months);
    return result;
}

function resizeRequestRangeBgFilled(groupSelector) {
    var requestRangeInput_value = document.querySelector(groupSelector + ' .request-range-input').value;
    var requestRangeInput_max = document.querySelector(groupSelector + ' .request-range-input').getAttribute('max');
    var percent = (requestRangeInput_value / requestRangeInput_max) * 93;
    var element = document.querySelector(groupSelector + ' .request-range-bg-filled');
    element.style.width = percent + '%';
}

function updateDetails() {
    var amount = parseInt(document.querySelector('#request-group-1 .request-range-input').value);
    var term = parseInt(document.querySelector('#request-group-2 .request-range-input').value);
    // var interest = (amount/0.65 + (0.025*amount*term)/2) - amount;
        var interest = 2.5/100;
    var total = amount + interest;
/* interes 2.5/100 para que sea decima*/
    // amount
    // document.querySelector('#request-calculation-value-1').innerHTML = 'RD$ ' + formatMoney(amount);
    // interest
    document.querySelector('#request-calculation-value-2').innerHTML = 'RD$ ' + formatMoney(interest);
    // total
    document.querySelector('#request-calculation-value-3').innerHTML = 'RD$ ' + formatMoney(amount + interest);
    // end date
    var date = addMonths(new Date(), parseInt(term));
    document.querySelector('#request-calculation-value-4').innerHTML =
        date.getDate() + '/' + pad((date.getMonth()+1), 2) + '/' + date.getFullYear();
}

function updateRequestCounter(groupSelector) {
    var requestRangeInput_value = document.querySelector(groupSelector + ' .request-range-input').value;
    var element = document.querySelector(groupSelector + ' .request-counter');
    element.innerHTML = requestRangeInput_value;
    // Update details at bottom as well
    updateDetails();
}

function tweakMinus(groupSelector) {
    var requestRangeInput_value = parseInt(document.querySelector(groupSelector + ' .request-range-input').value);
    var requestRangeInput_step = parseInt(document.querySelector(groupSelector + ' .request-range-input').getAttribute('step'));
    requestRangeInput_value -= requestRangeInput_step;
    document.querySelector(groupSelector + ' .request-range-input').value = requestRangeInput_value;
    updateRequestCounter(groupSelector);
    resizeRequestRangeBgFilled(groupSelector);
}

function tweakPlus(groupSelector) {
    var requestRangeInput_value = parseInt(document.querySelector(groupSelector + ' .request-range-input').value);
    var requestRangeInput_step = parseInt(document.querySelector(groupSelector + ' .request-range-input').getAttribute('step'));
    requestRangeInput_value += requestRangeInput_step;
    document.querySelector(groupSelector + ' .request-range-input').value = requestRangeInput_value;
    updateRequestCounter(groupSelector);
    resizeRequestRangeBgFilled(groupSelector);
}

function filterNumberField(event) {
    console.log(event.charCode);
    return (event.charCode >= 48 && event.charCode <= 57) ||
            event.charCode == 46 || event.charCode == 8 ||
           (event.charCode >= 37 && event.charCode == 40) ||
            event.charCode===0 || event.charCode == 45;
}

function validateForm() {
    var isValid = true;
    if(document.querySelector('#request-nombre').value == '') {
        addClass(document.querySelector('#request-nombre').parentNode, 'error');
        isValid = false;
    }
    if(document.querySelector('#request-telefono').value == '') {
        addClass(document.querySelector('#request-telefono').parentNode, 'error');
        isValid = false;
    }
    return isValid;
}

function requestRangeInput1() {
    resizeRequestRangeBgFilled('#request-group-1');
    updateRequestCounter('#request-group-1');
}
function requestRangeInput2() {
    resizeRequestRangeBgFilled('#request-group-2');
    updateRequestCounter('#request-group-2');
}

function trackClick(eventName) {
    // console.log('tracking click:', eventName);
    ga('send', 'event', {
        eventCategory: eventName,
        eventAction: 'click'
    });
}

// INIT
resizeRequestRangeBgFilled('#request-group-1');
resizeRequestRangeBgFilled('#request-group-2');
updateRequestCounter('#request-group-1');
updateRequestCounter('#request-group-2');
updateDetails();

// EVENT LISTENERS
document.querySelector('#request-group-1 .request-range-input').addEventListener('mouseover', function() { document.querySelector('#request-group-2').style.opacity = '0.5'; });
document.querySelector('#request-group-1 .request-range-input').addEventListener('mouseout', function() { document.querySelector('#request-group-2').style.opacity = '1'; });
document.querySelector('#request-group-2 .request-range-input').addEventListener('mouseover', function() { document.querySelector('#request-group-1').style.opacity = '0.5'; });
document.querySelector('#request-group-2 .request-range-input').addEventListener('mouseout', function() { document.querySelector('#request-group-1').style.opacity = '1'; });
document.querySelector('#request-group-1 .request-range-input').addEventListener('input', function() { requestRangeInput1(); });
document.querySelector('#request-group-1 .request-range-input').addEventListener('change', function() { requestRangeInput1(); });
document.querySelector('#request-group-2 .request-range-input').addEventListener('input', function() { requestRangeInput2(); });
document.querySelector('#request-group-2 .request-range-input').addEventListener('change', function() { requestRangeInput2(); });
document.querySelector('#request-group-1 .tweak-minus').addEventListener('click', function() { tweakMinus('#request-group-1'); });
document.querySelector('#request-group-2 .tweak-minus').addEventListener('click', function() { tweakMinus('#request-group-2'); });
document.querySelector('#request-group-1 .tweak-plus').addEventListener('click', function() { tweakPlus('#request-group-1'); });
document.querySelector('#request-group-2 .tweak-plus').addEventListener('click', function() { tweakPlus('#request-group-2'); });
document.querySelector('#request-submit').addEventListener('click', function() {
    trackClick('Obten Tu Oferta button');
    var blocker = document.body.appendChild(document.createElement('div'));
    blocker.className = 'loading';
    blocker.innerHTML = '<div class="loader"></div>';
    if(validateForm()) {
        var amount = 0;
        var term = 0;
        var name = '';
        var phone = '';
        amount = document.querySelector('#request-range-amount').value;
        term = document.querySelector('#request-range-term').value;
        name = document.querySelector('#request-nombre').value.trim();
        phone = document.querySelector('#request-telefono').value.trim();
        jQuery.ajax({
            url: 'https://www.ibanonline.com.do/web/slider_form.php',
            method: 'POST',
            data: {
                amount: amount,
                term: term,
                name: name,
                phone: phone
            },
            success: function(data, textStatus) {
                // document.body.removeChild(blocker);
                // console.log(textStatus);
                // console.log(data);
                // console.log('redirect');
                window.location.href = 'https://www.ibanonline.com.do/web/?monto='+amount+'&plazo='+term+'&nombre='+name+'&telefono='+phone+'';
            },
            error: function() {
                console.log('xhr error');
                // document.getElementById('submit_message').innerHTML = ibanform.submit_errormessage;
                // ibanform.unblocksubmit();
            }
        });
    } else {
        document.body.removeChild(blocker);
    }
});
document.querySelector('#request-nombre').addEventListener('click', function() {
    trackClick('Nombre completo field');
});
document.querySelector('#request-telefono').addEventListener('click', function() {
    trackClick('Numero de telefono field');
});
document.querySelector('#request-nombre').addEventListener('input', function() {
    var input = document.querySelector('#request-nombre');
    if(input.value != '') {
        removeClass(input.parentNode, 'error');
    } else {
        addClass(input.parentNode, 'error');
    }
});
document.querySelector('#request-telefono').addEventListener('input', function() {
    var input = document.querySelector('#request-telefono');
    if(input.value != '') {
        removeClass(input.parentNode, 'error');
    } else {
        addClass(input.parentNode, 'error');
    }
});
